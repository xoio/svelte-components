import type {Meta, StoryObj} from '@storybook/svelte';
import Menu from './menu.svelte';

const meta = {
    title: 'Menu',
    component: Menu,
    // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
    tags: ['autodocs'],
    parameters: {
        // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
        layout: 'fullscreen',
        backgrounds: {
            default: 'dark',
            values: [
                {
                    name: "dark", value: "rgb(28, 29, 32)"
                }
            ]
        }
    },
} satisfies Meta<Menu>;

export default meta;

type Story = StoryObj<typeof meta>;
export const View: Story = {}

