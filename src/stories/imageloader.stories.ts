import type {Meta, StoryObj} from '@storybook/svelte';
import Loader from "./imageloader.svelte";

const meta = {
    title: 'Loader',
    component: Loader,
    // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
    tags: ['autodocs'],
    parameters: {
        // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
        layout: 'fullscreen',
        backgrounds: {
            default: 'dark',
            values: [
                {
                    name: "dark", value: "rgb(28, 29, 32)"
                }
            ]
        }
    },
} satisfies Meta<Loader>;

export default meta;

type Story = StoryObj<typeof meta>;
export const View: Story = {}

