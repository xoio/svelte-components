import type {Meta, StoryObj} from '@storybook/svelte';
import Nav from './nav.svelte';

const meta = {
    title: 'UI/Nav',
    component: Nav,
    // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
    tags: ['autodocs'],
    argTypes:{
        urls:{
            control:"object",
        }
    },
    parameters: {
        // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
        layout: 'fullscreen',
        backgrounds: {
            default: 'dark',
            values: [
                {
                    name: "dark", value: "rgb(28, 29, 32)"
                }
            ]
        }
    },
} satisfies Meta<Nav>;

export default meta;
type Story = StoryObj<typeof meta>;
export const View: Story = {}

