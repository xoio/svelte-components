import {readable} from "svelte/store";

// Adapted from virtual-scroll package
// https://github.com/ayamflow/virtual-scroll
// Adapted for Svelte
const options = {
    mouseMultiplier: 1,
    touchMultiplier: 2,
    firefoxMultiplier: 15,
    keyStep: 120,
    preventTouch: false,
    unpreventTouchClass: 'vs-touchmove-allowed',
    useKeyboard: true,
    useTouch: true
}

/**
 * Readable store for knowing when the scroll wheel is activated.
 *
 * Some portions adapted from virtual-scroll package
 * https://github.com/ayamflow/virtual-scroll
 */
export const ScrollReader = readable({
    y: 0,
    deltaX: 0,
    deltaY: 0,
    direction: "",
    e: undefined,
    isScrolling: false
}, set => {

    let lastScrollTop = 0;
    let support = getSupport();

    if (support.hasWheelEvent) {

        window.addEventListener("wheel", wheelData)
    }

    function wheelData(e: any) {
        let deltaX = e.deltaX * -1
        let deltaY = e.deltaY * -1

        // get direction
        let direction = ""
        let st = window.pageYOffset || document.documentElement.scrollTop;
        if (deltaY > 0) {
            direction = "down"
        } else {
            direction = "up"
        }

        // for our purpose deltamode = 1 means user is on a wheel mouse, not touch pad
        // real meaning: https://developer.mozilla.org/en-US/docs/Web/API/WheelEvent#Delta_modes
        if (support.isFirefox && e.deltaMode === 1) {
            deltaX *= options.firefoxMultiplier
            deltaY *= options.firefoxMultiplier
        }

        deltaX *= options.mouseMultiplier
        deltaY *= options.mouseMultiplier

        lastScrollTop = st <= 0 ? 0 : st;

        set({
            deltaY: deltaY,
            deltaX: deltaX,
            y: st,
            direction: direction,
            //@ts-ignore
            e: e,
            isScrolling: true
        })
    }

    return function stop() {
        set({
            y: 0,
            deltaX: 0,
            deltaY: 0,
            direction: "",
            e: undefined,
            isScrolling: false
        })
        window.removeEventListener("wheel", wheelData)
    }
})


function getSupport() {
    return {
        hasWheelEvent: 'onwheel' in document,
        hasMouseWheelEvent: 'onmousewheel' in document,
        hasTouch: 'ontouchstart' in document,
        // @ts-ignore
        hasTouchWin: navigator.msMaxTouchPoints && navigator.msMaxTouchPoints > 1,

        // @ts-ignore
        hasPointer: !!window.navigator.msPointerEnabled,
        hasKeyDown: 'onkeydown' in document,
        isFirefox: navigator.userAgent.indexOf('Firefox') > -1,
    }
}
