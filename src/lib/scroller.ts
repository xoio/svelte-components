/**
 * A basic class to help managed eased scrolling. It's based on the idea that you want to scroll a group of items
 * within a parent container.
 *
 * Note that this does not handle events but leaves that up to the user as to how best to handle.
 */
export default class Scroller {
    currentY: number = 0
    targetY: number = 0
    container_max: number = 0
    mouseDown: boolean = false
    parentElement: HTMLElement
    ease: number = 0.02
    anim_ref: any

    constructor(parent: HTMLElement) {
        this.parentElement = parent;
        this.container_max = parent.scrollHeight
    }

    /**
     * Sets the maximum scroll height. Based on parent element max, but you can pass in your own value as well
     * @param max {number} a number > -1 for a custom scroll height
     */
    setMaxHeight(max: number = -1) {
        if (max === -1) {
            this.container_max = this.parentElement.scrollHeight
        } else {
            this.container_max = max;
        }
    }

    /**
     * Sets whether or not the mouse is currently being pressed.
     * @param down
     */
    setMouseDown(down: boolean = false) {
        this.mouseDown = down;
        return this
    }

    /**
     * Updates target value on event
     * @param e {*} the event object from whatever is handling your scrolling.
     */
    onMovement(e: any) {

        // TODO not sure if theres a generic wheel / scroll event but for now, assume deltaY is a prop
        this.targetY += e["deltaY"]
        this.targetY = Math.min(0, this.targetY)
        this.targetY = Math.max(this.container_max * -1, this.targetY)
    }

    /**
     * Stops animation.
     */
    stopAnimate() {
        cancelAnimationFrame(this.anim_ref)
    }

    animate() {
        this.anim_ref = requestAnimationFrame(this.animate.bind(this))

        if (!this.mouseDown) {
            this.currentY += (this.targetY - this.currentY) * this.ease
            this.parentElement.scrollTo(0, this.currentY * -1)
        } else {
            this.currentY = 0
        }
    }

}